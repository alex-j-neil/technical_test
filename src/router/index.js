import Vue from 'vue'
import Router from 'vue-router'
import Question from '@/components/Question'
import Start from '@/components/Start'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Start',
      component: Start
    },
    {
      path: '/question',
      name: 'Question',
      component: Question
    },
    {
      path: '/*',
      name: '404',
      component: {
        template: '<h1 style="text-align:center;margin-top:2em;">404</h1>'
      }
    }

  ]
})
